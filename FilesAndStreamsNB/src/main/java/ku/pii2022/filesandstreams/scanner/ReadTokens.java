/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.scanner;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author dave
 */
public class ReadTokens {
    public static void main(String[] args){
        File inFile = new File("files/numbersin.txt");
        try(Scanner scan = new Scanner(inFile)){
            int countTokens=0; // just for demo purposes
            // this effectively allows individual characters to be read in but as Strings
            scan.useDelimiter(""); 
            while(scan.hasNext()){
                String token = scan.next();
                countTokens++;
                // notice print not println i.e. no new lines
                // however new line is one of the tokens 
                System.out.print(token);
            }
           System.out.println();
           System.out.println("Read " + countTokens + " tokens");
        }
        catch(Exception e){
                   e.printStackTrace();
                }
            
        }
        
    }

