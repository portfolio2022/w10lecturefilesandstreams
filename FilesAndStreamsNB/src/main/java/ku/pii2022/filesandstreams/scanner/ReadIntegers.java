/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.scanner;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author dave
 */
public class ReadIntegers {
    public static void main(String[] args){
        File inFile = new File("files/numbersin.txt");
        try(Scanner scan = new Scanner(inFile)){
            int countNumbers=0; // just for demo purposes
            int total = 0;
            // this effectively allows individual characters to be read in but as Strings
            
            while(scan.hasNextInt()){
                int number = scan.nextInt();
                countNumbers++;
                total+=number;
                System.out.println(number);
            }
           System.out.println();
           System.out.println("Read " + countNumbers + " numbers adding up to " + total);
        }
        catch(Exception e){
                   e.printStackTrace();
                }
            
        }
        
    }

