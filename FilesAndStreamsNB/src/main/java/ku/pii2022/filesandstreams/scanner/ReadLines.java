/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.scanner;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author dave
 */
public class ReadLines {
    public static void main(String[] args){
        
        try(Scanner inFile = new Scanner(new File("files/numbersin.txt"));){
            int countLines=0; // just for demo purposes
            while(inFile.hasNextLine()){
                String line=inFile.nextLine();
                countLines++;
                System.out.println(line);
            }
           System.out.println("Read " + countLines + " lines");
        }
        catch(Exception e){
                   e.printStackTrace();
                }
            
        }
        
    }

