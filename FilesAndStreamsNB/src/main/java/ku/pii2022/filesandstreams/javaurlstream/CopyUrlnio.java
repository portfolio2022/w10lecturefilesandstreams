/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javaurlstream;

import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


/**
 *
 * @author dave
 */
public class CopyUrlnio {
    public static void main(String[] args){
        
        String inURL="http://kunet.kingston.ac.uk/~ku63026/CI5100/FilesAndStreams/files/wordsin.txt";
        Path outFile = Paths.get("files/wordsout.txt");
        try (InputStream inStream=new URI(inURL).toURL().openStream();){
            Files.copy(inStream, outFile, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(inURL + " copied to " + outFile );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
