/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javaurlstream;

import java.io.File;
import java.net.URI;
import java.util.Scanner;

/**
 *
 * @author dave
 */
public class ReadUrlCsvTokensScanner {
    public static void main(String[] args){
        String inURL="http://kunet.kingston.ac.uk/~ku63026/CI5100/FilesAndStreams/files/wordsin.csv";
        try(Scanner scan = new Scanner(new URI(inURL).toURL().openStream())){
            int countTokens=0; // just for demo purposes
            // use comma as delimiter for the read
            scan.useDelimiter(","); 
            while(scan.hasNext()){
                String token = scan.next();
                countTokens++;
                // notice println this time
                // new line is not one of the tokens 
                System.out.println(token);
            }
           System.out.println();
           System.out.println("Read " + countTokens + " tokens");
        }
        catch(Exception e){
                   e.printStackTrace();
                }
            
        }
        
    }

