/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javaurlstream;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.net.URI;

/**
 *
 * @author dave
 */
public class CopyUrlio {
  public static void main(String[] args){
      
      
      String inURL="http://kunet.kingston.ac.uk/~ku63026/CI5100/FilesAndStreams/files/numbersin.txt";
      String outFile="files/numbersout.txt";
      int countBytes=0; // just for demo purposes - not needed
      try( 
              
              InputStream inStream=new URI(inURL).toURL().openStream();
              BufferedInputStream bufStream=new BufferedInputStream(inStream);
              FileWriter fw=new FileWriter(outFile);
              BufferedWriter bw=new BufferedWriter(fw);
              ){
          // You could use a URL with a local file comment out these to see
          //String inFileStr="files/numbersin.txt";
          //File inFile = new File(inFileStr);
          //String localFileURL = inFile.toURI().toURL().toString();
          //System.out.println(localFileURL);
          
          int nextByte;
          // slightly different format of while loop to CopyFileChar
          // no ready() method could use available() but read returns -1 if end of stream reached
          while( (nextByte = bufStream.read()) != -1){
            
              countBytes++;
              bw.write((char)nextByte);
             
              // will read the new line character of an empty line unlike readLine
              // therefore the output file will be the same as the input file
          }
        System.out.println(inURL + " copied to " + outFile + " " + countBytes + " bytes");
      }
      catch(Exception e){
              e.printStackTrace(); // for debugging purposes would not be used in production code
              }
  }  
}
