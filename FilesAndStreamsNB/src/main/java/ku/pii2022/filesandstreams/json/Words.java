/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.json;

import java.util.List;

/**
 *
 * @author dave
 */
public class Words {
    
    private List<Word> words;

    public Words(List<Word> words) {
        this.words = words;
    }

    public Words() {
    }

    /**
     * Get the value of words
     *
     * @return the value of words
     */
    public List<Word> getWords() {
        return words;
    }

    /**
     * Set the value of words
     *
     * @param words new value of words
     */
    public void setWords(List<Word> words) {
        this.words = words;
    }

    @Override
    public String toString() {
        return "Words{" + "words=" + words + '}';
    }

    
}
