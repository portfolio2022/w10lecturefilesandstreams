/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dave
 */
public class JSONService {
    public static final String WORDS_FILE = System.getProperty("user.dir") + File.separator + "files"  + File.separator +  "wordsin.txt";
     public static final String JSON_FILE = System.getProperty("user.dir") + File.separator + "files"  + File.separator +  "words.json";
     public static final String JSON_WEB = "https://kunet.kingston.ac.uk/ku63026/CI5105/words.json";
    public static void main(String[] args){
       // Words words = new Words(populateModelFromTxt(JSONService.WORDS_FILE));
       //  System.out.println(words.toString());
       // writeToJsonFile(words);
      //  System.out.println(jsonStr);
       // Words deserializeWords = readFromJsonFile(JSON_FILE);
         Words deserializeWords = readFromJsonWebService(JSON_WEB);
        System.out.println(deserializeWords.toString() );
      
    }
    
    public static String writeToJsonStr(String str){
        Gson gson = new Gson();
        return gson.toJson(str);
    }
    
     public static String writeToJsonStr(List<String> listStr){
         Gson gson = new GsonBuilder().setPrettyPrinting().create();
        //Gson gson = new Gson();
        return gson.toJson(listStr);
    }
     
      public static String writeToJsonStr(Words words){
         Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(words);
    }
      
        public static void writeToJsonFile(Words words){
         Gson gson = new GsonBuilder().setPrettyPrinting().create();
         
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(JSON_FILE))){
            gson.toJson(words, bw);
        }
        catch(IOException e){
            // debugging only 
            e.printStackTrace();
        }
      
    }
        
        
         public static Words readFromJsonFile(String filename){
         Gson gson = new GsonBuilder().setPrettyPrinting().create();
        
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            return gson.fromJson(br, Words.class);
        }
        catch(IOException e){
            // debugging only 
            e.printStackTrace();
        }
     return null;
    }
         
          public static Words readFromJsonWebService(String url){
         Gson gson = new GsonBuilder().setPrettyPrinting().create();
        
        //try(BufferedReader br = new BufferedReader(new FileReader(url))){
        try(InputStreamReader isr  = new InputStreamReader(new URI(JSON_WEB).toURL().openStream())){
            return gson.fromJson(isr, Words.class);
        }
        catch(IOException e){
            // debugging only 
            e.printStackTrace();
        }
        catch(URISyntaxException e){
            // debugging only 
            e.printStackTrace();
        }
     return null;
    }
      
      
       
     
      public static Words readFromJsonStr(String jsonStr){
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, Words.class);
    }
    
        public static List<String> populateFromTxt(String filename){
            List<String> strList = new ArrayList();
            try(BufferedReader br = new BufferedReader(new FileReader(filename))){
                while(br.ready()){
                    strList.add(br.readLine());
                }
            }
            catch(IOException e){
                // just for debugging
                e.printStackTrace();
            }
           
            return strList;
        }
    
    
      public static List<Word> populateModelFromTxt(String filename){
            List<Word> wordList = new ArrayList();
            try(BufferedReader br = new BufferedReader(new FileReader(filename))){
                while(br.ready()){
                   wordList.add(new Word((br.readLine())));
                }
            }
            catch(IOException e){
                // just for debugging
                e.printStackTrace();
            }
           
            return wordList;
        }
}
