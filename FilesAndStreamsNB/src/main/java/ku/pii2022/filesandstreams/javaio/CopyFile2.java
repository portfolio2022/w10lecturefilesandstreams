/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javaio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author dave
 */
public class CopyFile2 {
  public static void main(String[] args){
      String inFile="files/numbersin.txt";
      String outFile="files/numbersout.txt";
      int countLines=0;
      try(
              FileReader fr=new FileReader(inFile);
              BufferedReader br=new BufferedReader(fr);
              FileWriter fw=new FileWriter(outFile);
              BufferedWriter bw=new BufferedWriter(fw);
              ){
          while(true){
              // a different way of reading until the end of the file
              // if the last line is empty then it will return null
              String line=br.readLine();
              countLines++;
              if(line == null){
                  break;
              }
              
              bw.write(line);
              // add a new line character to the file
              // this will add a new line if there is not one for the last line
              // difficult to check as readLine ignores the new line character
              // and null is returned whether there is an empty last line or not
              // therefore the output file may have an additional empty line at the end
              bw.newLine();
          }
          System.out.println(inFile + " copied to " + outFile + " " + countLines + " lines");
      }
      catch(Exception e){
              e.printStackTrace();
              }
  }  
}
