/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javaio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author dave
 */
public class CopyFileChar {
  public static void main(String[] args){
      String inFile="files/wordsin.txt";
      String outFile="files/wordsout.txt";
      int countChars=0; // just for demo purposes not needed
      try(
              FileReader fr=new FileReader(inFile);
              BufferedReader br=new BufferedReader(fr);
              FileWriter fw=new FileWriter(outFile);
              BufferedWriter bw=new BufferedWriter(fw);
              ){
          while(br.ready()){
              // will read the new line character of an empty line unlike readLine
              // therefore the output file will be the same as the input file
              char c=(char)br.read();
              bw.write(c);
              countChars++;
          }
        System.out.println(inFile + " copied to " + outFile + " " + countChars + " characters");

      }
      catch(Exception e){
              e.printStackTrace(); // for debugging purposes would not be used in production code
              }
  }  
}
