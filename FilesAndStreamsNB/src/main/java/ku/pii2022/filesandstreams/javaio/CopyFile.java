/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javaio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author dave
 */
public class CopyFile {
  public static void main(String[] args){
      String inFile="files/numbersin.txt";
      String outFile="files/numbersout.txt";
      int countLines=0; // just for demo purposes - not needed
      try(
              FileReader fr=new FileReader(inFile);
              BufferedReader br=new BufferedReader(fr);
              FileWriter fw=new FileWriter(outFile);
              BufferedWriter bw=new BufferedWriter(fw);
              ){
          while(br.ready()){
              // if the last line is empty then it will not be read
              String line=br.readLine();
              countLines++;
              bw.write(line);
              bw.newLine();
              // add a new line character to the file
              // this will add a new line if there is not one for the last line
              // difficult to check as readLine ignores the new line character
              // and null is returned whether there is an empty last line or not
              // therefore the output file may have an additional empty line at the end
          }
        System.out.println(inFile + " copied to " + outFile + " " + countLines + " lines");
      }
      catch(Exception e){
              e.printStackTrace(); // for debugging purposes would not be used in production code
              }
  }  
}
