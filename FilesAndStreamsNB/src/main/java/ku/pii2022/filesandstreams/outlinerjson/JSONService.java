/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.outlinerjson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;




/**
 *
 * @author dave
 */
public class JSONService {
    public static final String JSON_WEB = "https://kunet.kingston.ac.uk/ku63026/CI5105/outline.json";
    public static void main(String[] args){
     Document d = readFromJsonWebService(JSON_WEB);
     System.out.println(d);
      
    }
    
    
       public static Document readFromJsonWebService(String url){
         Gson gson = new GsonBuilder().setPrettyPrinting().create();
        
       
        try(InputStreamReader isr  = new InputStreamReader(new URI(JSON_WEB).toURL().openStream())){
            return gson.fromJson(isr, Document.class);
        }
        catch(IOException e){
            // debugging only 
            e.printStackTrace();
        }
        catch(URISyntaxException e){
            // debugging only 
            e.printStackTrace();
        }
     return null;
    }
}
