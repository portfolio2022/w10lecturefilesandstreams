/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.outlinerjson;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 *
 * @author dave
 */
public class Section {
     private String tag;
    private String person;
     private String id;
     
     @SerializedName(value = "content", alternate = "title")
      private String content;
     
     @SerializedName(value = "subsections", alternate = "subsection")
       private List<Section> subsections;

    public Section(String id, String content) {
        this.id = id;
        this.content = content;
    }

    public Section() {
    }

    @Override
    public String toString() {
        return "Section{" + "tag=" + tag + ", id=" + id + ", content=" + content + ", subsections=" + subsections + '}';
    }

   


    /**
     * Get the value of subsections
     *
     * @return the value of subsections
     */
    public List<Section> getSubsections() {
        return subsections;
    }

    /**
     * Set the value of subsections
     *
     * @param subsections new value of subsections
     */
    public void setSubsections(List<Section> subsections) {
        this.subsections = subsections;
    }

   

    /**
     * Get the value of tag
     *
     * @return the value of tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * Set the value of tag
     *
     * @param tag new value of tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

  

    /**
     * Get the value of person
     *
     * @return the value of person
     */
    public String getPerson() {
        return person;
    }

    /**
     * Set the value of person
     *
     * @param person new value of person
     */
    public void setPerson(String person) {
        this.person = person;
    }

   
    /**
     * Get the value of content
     *
     * @return the value of content
     */
    public String getContent() {
        return content;
    }

    /**
     * Set the value of content
     *
     * @param content new value of content
     */
    public void setContent(String content) {
        this.content = content;
    }

   

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(String id) {
        this.id = id;
    }

}
