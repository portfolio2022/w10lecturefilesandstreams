/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.outlinerjson;

import java.util.List;

/**
 *
 * @author dave
 */
public class Document {
    
    private List<Section> sections;

    public Document() {
    }

    @Override
    public String toString() {
        return "Document{" + "sections=" + sections + '}';
    }

    
    
    /**
     * Get the value of sections
     *
     * @return the value of sections
     */
    public List<Section> getSections() {
        return sections;
    }

    /**
     * Set the value of sections
     *
     * @param sections new value of sections
     */
    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

}
