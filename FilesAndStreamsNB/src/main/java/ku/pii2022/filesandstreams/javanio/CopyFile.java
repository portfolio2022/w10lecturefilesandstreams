/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javanio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


/**
 *
 * @author dave
 */
public class CopyFile {
    public static void main(String[] args){
        Path inFile  = Paths.get("files/numbersin.txt");
        Path outFile = Paths.get("files/numbersout.txt");
        
        try {
            Files.copy(inFile, outFile, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(inFile + " copied to " + outFile );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
