/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javanio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;


/**
 *
 * @author dave
 */
public class CopyFileBytes {
    public static void main(String[] args){
        Path inFile  = Paths.get("files/numbersin.txt");
        Path outFile = Paths.get("files/numbersout.txt");
        
        try {
            byte[] allBytes = Files.readAllBytes(inFile);
             // will read the new line character byte of an empty line unlike readAllLines
              // therefore the output file will be the same as the input file
            Files.deleteIfExists(outFile);
            Files.write(outFile, allBytes, StandardOpenOption.CREATE);
            System.out.println(inFile + " copied to " + outFile + " " + allBytes.length + " bytes" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
