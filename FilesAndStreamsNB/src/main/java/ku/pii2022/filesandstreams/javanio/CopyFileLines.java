/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.javanio;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;


/**
 *
 * @author dave
 */
public class CopyFileLines {
    public static void main(String[] args){
        Path inFile  = Paths.get("files/numbersin.txt");
        Path outFile = Paths.get("files/numbersout.txt");
        
        try {
            List<String> listStr= Files.readAllLines(inFile);
            // even the built-in String line reader adds the extra new line if it does not exist in the file
            Files.deleteIfExists(outFile);
            Files.write(outFile, listStr, StandardOpenOption.CREATE);
            System.out.println(inFile + " copied to " + outFile + " " + listStr.size() + " lines" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
