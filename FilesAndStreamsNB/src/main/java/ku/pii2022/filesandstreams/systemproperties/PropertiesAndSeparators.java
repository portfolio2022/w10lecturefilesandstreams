/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.systemproperties;

import java.io.File;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author dave
 */
public class PropertiesAndSeparators {
    public static void main(String[] args){
        allSystemProperties();
     //   pathSeparator();
    // fileSeparator();
    //lineSeparator();
    }
    
    public static void pathSeparator(){
        // when more than one file path is required
        // e.g. java.library.path etc
        // different ways to get this
        String pathSepStr1 = System.getProperty("path.separator");
        String pathSepStr2 = File.pathSeparator;
        char pathSepChar = File.pathSeparatorChar;
        System.out.println("pathSepStr1= " + pathSepStr1);
        System.out.println("pathSepStr2= " + pathSepStr2);
        System.out.println("pathSepChar= " + pathSepChar);
    }
    
     public static void fileSeparator(){
        // delimits the different parts of a file path
        // unix version "/" should work on all OS
        // but safest is to use the System property when building a file path
        String fileSepStr1 = System.getProperty("file.separator");
        String fileSepStr2 = File.separator;
        char fileSepChar = File.separatorChar;
        System.out.println("fileSepStr1= " + fileSepStr1);
        System.out.println("fileSepStr2= " + fileSepStr2);
        System.out.println("fileSepChar= " + fileSepChar);
    }
     
      public static void lineSeparator(){
        // provides a new line 
        // most "/n" should work but it is not guaranteed
        //  safest is to use the System property when building or splitting Strings with or at new line
        String lineSepStr1 = System.getProperty("line.separator");
        String lineSepStr2 = System.lineSeparator();
        //char[] chars = lineSepStr2.toCharArray();
        //for(char c: chars){
        //    System.out.print(String.format("%c", (int)c));
        //}
        
        // these just print a new line - why?!
        System.out.print("lineSepStr1= " + lineSepStr1);
        System.out.print("lineSepStr2= " + lineSepStr2);
        // if we want to check what it actually is
        System.out.println("new line= " + System.lineSeparator().replace("\r", "\\r").replace("\n","\\n"));
        // this is what the new line string is actually made up of for your system
        // a bit clunky 
        // here is the Apache Commons StringEscapeUtils version which will work with any special escape characters
        System.out.println("new line= " + StringEscapeUtils.escapeJava(System.lineSeparator()));

        
    }
    
    public static void allSystemProperties(){
        // this is a HashTable with a key and value
        // the key and value are Objects but they are Strings
        
        Properties properties = System.getProperties();
        
        Set propertyKeys = properties.keySet();
        for(Object key: propertyKeys){
            String keyStr=key.toString();
            String value= properties.getProperty(keyStr);
            System.out.println(keyStr + " : " + value);
        }
    }
}
