/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.commonsio;

import java.io.File;
import org.apache.commons.io.FileUtils;


/**
 *
 * @author dave
 */
public class CopyFileBytes {
    public static void main(String[] args){
        File inFile  = new File("files/numbersin.txt");
        File outFile = new File("files/numbersout.txt");
        
        try {
                    // similar to java.nio - could well have influenced its development
            byte[] allBytes = FileUtils.readFileToByteArray(inFile);
             // will read the new line character byte of an empty line unlike readLines
              // therefore the output file will be the same as the input file
            FileUtils.writeByteArrayToFile(outFile, allBytes);
            System.out.println(inFile + " copied to " + outFile + " " + allBytes.length + " bytes" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
