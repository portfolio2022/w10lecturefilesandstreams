/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.commonsio;

import java.io.File;
import org.apache.commons.io.FileUtils;


/**
 *
 * @author dave
 */
public class CopyFile {
    public static void main(String[] args){
        File inFile  = new File("files/numbersin.txt");
        File outFile = new File("files/numbersout.txt");
        // similar to java.nio - could well have influenced its development
        try {
            FileUtils.copyFile(inFile, outFile);
            System.out.println(inFile + " copied to " + outFile );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
