/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ku.pii2022.filesandstreams.commonsio;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;
import org.apache.commons.io.FileUtils;


/**
 *
 * @author dave
 */
public class CopyFileLines {
    public static void main(String[] args){
        File inFile  = new File("files/numbersin.txt");
        File outFile = new File("files/numbersout.txt");
        
        try {
            // no javadoc for this - hence strikethrough (maybe deprecated?)
          //  List<String> listStr= FileUtils.readLines(inFile);
            List<String> listStr= FileUtils.readLines(inFile,Charset.defaultCharset());
           
            // even the built-in String line reader adds the extra new line if it does not exist in the file
            FileUtils.writeLines(outFile, listStr);
            System.out.println(inFile + " copied to " + outFile + " " + listStr.size() + " lines" );
            System.out.println("Default charset " + Charset.defaultCharset().name() );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
